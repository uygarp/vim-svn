if expand("%:e") == "svnlog"
    set nonumber
"    syn match myKeyWord "Changed" contained
"    hi kwRed  term=standout ctermfg=12 guifg=Red
"    hi def link  myKeyWord  kwRed
    hi cRed ctermbg=black guibg=red guifg=red ctermfg=red
    hi cGreen ctermbg=black guibg=red guifg=green ctermfg=green
    hi cBlue ctermbg=black guibg=red guifg=blue ctermfg=blue

    "call matchadd('CustomPink', '\<Changed\>')
    call matchadd('cGreen', '   M ')
    call matchadd('cBlue', '   A ')
    call matchadd('cRed', '   D ')

    noremap <CR> :call MyFunc()<CR>
    function! MyFunc()
        let currentLine = getline(".")
        let pattern = '^r\d\+'
        let repo_root = "http://svn/repos/SingleChipTV"
        if currentLine =~ "   M "
            let _line = line(".")
            while _line >= 1
                if getline(_line) =~# '^r\d\+'
                    let revision = matchlist(getline(_line), pattern)[0][1:]
                    let current_file = repo_root . currentLine[5:]
                    let shellcmd = "svn diff -r".(revision-1).":".revision." ".current_file
                    " echo shellcmd
                    execute ':silent !'.shellcmd
                    execute ':redraw!'
                    break
                endif
                let _line -= 1
            endwhile
        endif
        echo ""
    endfunction
endif

